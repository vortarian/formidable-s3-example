FROM node:18-alpine
LABEL authors="vortarian"


RUN mkdir -p /app

WORKDIR /app

COPY . /app

RUN yarn install

ENTRYPOINT ["yarn", "run", "server"]
