# Streaming s3 Example

Simple server example that can stream a large object to s3 without caching it locally

To use this, setup a AWS profile called "demo" in ${HOME}/.aws
Run the commands below to start the server
```bash
docker build -t node-stream-s3 . 
AWS_PROFILE=demo aws s3 mb s3://demo-s3-test-bucket;
docker run -it -v ${HOME}/.aws:/root/.aws:ro -e AWS_PROFILE=demo -p 3000:3000 -e AWS_S3_BUCKET=demo-s3-test-bucket -e AWS_SDK_LOAD_CONFIG=1 -m 300m node-stream-s3
```

To upload a very large file:
```bash
curl -o - -v http://localhost:3000/api/upload -F title=curltest -F file=@./hiveos-0.6-219-stable@220928.img
```

Note that the node process uses ~130 mb of memory in total.  This is not considered a hardened example for obvious reasons.