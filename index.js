import http from 'node:http';
import { PassThrough } from 'node:stream';
import AWS from 'aws-sdk';
import formidable from 'formidable';



const s3Client = new AWS.S3();
const s3Bucket = process.env.AWS_S3_BUCKET;
const uploadStream = (file) => {
    const pass = new PassThrough();
    s3Client.upload(
        {
            Bucket: s3Bucket,
            Key: file.newFilename,
            Body: pass,
        },
        (err, data) => {
            console.log(err, data);
        },
    );

    return pass;
};

const server = http.createServer({
    headersTimeout: 3000,
    keepAlive: true,
    requestTimeout: 1000 * 60 * 30
}, (req, res) => {
    if (req.url === '/api/upload' && req.method.toLowerCase() === 'post') {
        // parse a file upload
        const form = formidable({
            fileWriteStreamHandler: uploadStream,
            maxFileSize: 10000000000,
            maxTotalFileSize: 10000000000
        });

        form.parse(req, (err, fields, files) => {
            if (err) {
                console.error(err);
                res.writeHead(err.httpCode || 400, { 'Content-Type': 'text/plain' });
                res.end(String(err));
                return;
            } else {
                res.writeHead(200);
                res.end();
            }
        });

        return;
    }

    // show a file upload form
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(`
    <h2>With Node.js <code>"http"</code> module</h2>
    <form action="/api/upload" enctype="multipart/form-data" method="post">
      <div>Text field title: <input type="text" name="title" /></div>
      <div>File: <input type="file" name="file"/></div>
      <input type="submit" value="Upload" />
    </form>
  `);
});

server.listen(3000, () => {
    console.log('Server listening on http://localhost:3000 ...');
});